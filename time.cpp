#include <iostream>
#include <iomanip>
#include <cmath>
 //Немного сделал по другому,программа выводит время нынешнее
class SimpleTime {
 public:
  SimpleTime() : time(0) {}
  SimpleTime(double time) : time(time) {}
  SimpleTime(int hours, int minutes, double seconds)
    : time(hours * 3600 + minutes * 60 + seconds) {}
  int getHours() const {
    return int(time) / 3600;
  }
  int getMinutes() const {
    return std::abs(int(time) % 3600) / 60;
  }
  double getSeconds() const {
    return std::abs(time) - std::abs(getHours()) * 3600 - getMinutes() * 60;
  }
  double asDouble() const {
    return time;
  }
 private:
  double time;
};
 
SimpleTime operator+(const SimpleTime &a, const SimpleTime &b) {
  return SimpleTime(a.asDouble() + b.asDouble());
}
 
SimpleTime operator-(const SimpleTime &a, const SimpleTime &b) {
  return SimpleTime(a.asDouble() - b.asDouble());
}
 
int compare(const SimpleTime &a, const SimpleTime &b, const double epsilon = 0.00001) {
  return ((std::fabs(a.asDouble() - b.asDouble()) < epsilon) ? 0 :
    (a.asDouble() < b.asDouble()) ? -1 : 1);
}
 
bool operator==(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) == 0;
}
 
bool operator!=(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) != 0;
}
 
bool operator<(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) < 0;
}
 
bool operator>(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) > 0;
}
 
bool operator<=(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) <= 0;
}
 
bool operator>=(const SimpleTime &a, const SimpleTime &b) {
  return compare(a, b) >= 0;
}
 
std::ostream &operator<<(std::ostream &stream, const SimpleTime &time) {
  int hours = time.getHours();
  return stream <<
    std::setfill('0') << std::setw(2) <<
    ((hours % 12 == 0) ? 12 : hours % 12) << ":" << 
    std::setfill('0') << std::setw(2) <<
    time.getMinutes() << ":" << 
    std::setfill('0') << std::setw(6) <<
    std::fixed << std::setprecision(3) <<
    time.getSeconds() << " " <<
    ((hours % 24 >= 12) ? "pm" : "am");
    
}
 
int main(int, char**) {
  std::cout << (SimpleTime(-18, -1, -0.54) + SimpleTime(-32, -18, -10)) << std::endl;
  std::cout << (SimpleTime(-18, -1, -0.54) > SimpleTime(-32, -18, -10)) << std::endl;
}
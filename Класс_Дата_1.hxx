#ifndef DATE_H
#define DATE_H
 
#include <stdio.h>
 
int LongMonth[] = {1,3,5,7,8,10,12};
char * sMonth[] = 
{
    "января",
    "февраля",
    "марта",
    "апреля",
    "мая",
    "июня",
    "июля",
    "августа",
    "сентября",
    "октября",
    "ноября",
    "декабря"
};
 
class date
{
private:
    int day, month, year;
    bool bChangeDay,bChangeMonth,bChangeYear;//Флаги указывающие на корректировку 
    char sDate[256];//Дата в строковом формате
public:
    date();                               //конструктор без параметров
    date(int iDay, int iMonth, int iYear);//конструктор с параметрами
    date(const date &val);                //конструктор копирования
    ~date();                              //деструктор
 
    int getDay()    {return day;}
    int getMonth()  {return month;}
    int getYear()   {return year;} 
 
    void setDay(int iDay); 
    void setMonth(int iMonth);
    void setYear(int iYear); 
    //Сигнализируют пользователю о некрректности ввода даты
    bool IsChangeDay(){return bChangeDay;}
    bool IsChangeMonth(){return bChangeMonth;}
    bool IsChangeYear(){return bChangeYear;}
 
    char * ShowDate(int ifmt);
};
 
date::date()
{
    day   = 1;
    month = 1;
    year  = 1900;
    
    bChangeDay   = false;
    bChangeMonth = false;
    bChangeYear  = false;
 
    sDate[0] = '\0';
}
 
date::date(int iDay, int iMonth, int iYear)
{
    bChangeDay   = false;
    bChangeMonth = false;
    bChangeYear  = false;
 
    setDay(iDay);
    setMonth(iMonth);
    setYear(iYear);
 
    sDate[0] = '\0';
}
 
date::date(const date &val)
{
    day   = val.day;
    month = val.month;
    year  = val.year;
 
    sDate[0] = '\0';
}
 
date::~date()
{
    //Память не выделял, так что думаю, 
    //кроме установки дефолтных значений 
    //ничего сделать и не можем
    day   = 1;
    month = 1;
    year  = 1900;
 
    sDate[0] = '\0';
}
 
void date::setDay(int iDay)
{
    if(iDay < 1 || 31 < iDay)
        bChangeDay   = true;
    if(bChangeDay)
        day = 1;
    day = iDay;
}
 
void date::setMonth(int iMonth)
{
    int i, nMonth = sizeof(LongMonth)/sizeof(int);
    if(iMonth < 1 || 12 < iMonth)
        bChangeMonth = true;
    if(bChangeMonth)
        month = 1;
    //Если месяц не 31-дневный, 
    //а day == 31,
    //то устанавливаем day == 1
    if(day == 31)
    {
        for(i = 0; i < nMonth; i++)
        {
            if(iMonth == LongMonth[i])
                break;
        }
        if(nMonth <= i)
            bChangeDay = true;
    }
    if(bChangeDay)
        day = 1;
    month = iMonth;
}
 
void date::setYear(int iYear)
{
    if(iYear < 1)
        bChangeYear = true;
    if(bChangeYear)
        iYear = 1;
    if(28 < day && month == 2)
    if(iYear % 4 != 0)
        bChangeDay = true;
    if(bChangeDay)
        day = 1;
    year = iYear;
}
 
char * date::ShowDate(int ifmt)
{
    switch(ifmt)
    {
        case 1 :
            sprintf(sDate,"%02d %s %04d\r\n",day,sMonth[month - 1],year);
            break;
        default :
            sprintf(sDate,"%02d.%02d.%04d\r\n",day,month,year);
            break;
    }
    return &sDate[0];
}
 
#endif